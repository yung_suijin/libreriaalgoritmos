namespace libreriaAlgoritmos.Entidades
{
    public class Sucursal
    {
        public string IDEstado { get; set; }
        public string IDSucursal { get; set; }
        public string NombreEstado { get; set; }
        public string NombreSucursal { get; set; }
    }
}