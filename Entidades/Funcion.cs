using System;

namespace libreriaAlgoritmos.Entidades
{
    public class Funcion : IComparable
    {
        public int IDSala { get; set; }
        public int IDPelicula { get; set; }
        public string HorarioInicio { get; set; }
        public string HorarioFinal { get; set; }
        public string NombrePelicula { get; set; }
        public string NombreSucursal { get; set; }
        public string Fecha { get; set; }

        public int CompareTo(object obj)
        {
            var funcion = (Funcion)obj;
            int ordenar = Fecha.CompareTo(funcion.Fecha);

            return (ordenar == 0) ? HorarioInicio.CompareTo(funcion.HorarioInicio) : ordenar;
        }
    }
}