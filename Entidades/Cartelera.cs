namespace libreriaAlgoritmos.Entidades
{
    public class Cartelera
    {
        public string NombrePelicula { get; set; }
        public int IDSala { get; set; }
        public string Fecha { get; set; }
        public string HorarioInicio { get; set; }
        public string HorarioFinal { get; set; }
        public string Director { get; set; }
        public string Productor { get; set; }
        public int Duracion { get; set; }
    }
}