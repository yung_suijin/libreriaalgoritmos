using System;

namespace libreriaAlgoritmos.Entidades
{
    public class Pelicula
    {
        public int IDPelicula { get; set; }
        public string NombrePelicula { get; set; }
        public string Director { get; set; }
        public string Productor { get; set; }
        public string Clasificacion { get; set; }
        public int Duracion { get; set; }
        public string Genero { get; set; }
    }
}