using System;
using System.Data;
using System.Data.SqlClient;

namespace libreriaAlgoritmos
{
    internal class ConexionDb
    {
        private SqlConnection conexionDb { get; set; }

        public ConexionDb(string connectionString)
        {
            conexionDb = new SqlConnection(connectionString);
        }

        public bool AbrirConexion()
        {
            try
            {
                conexionDb.Open();
                return conexionDb.State == System.Data.ConnectionState.Open;
            }
            catch(SqlException)
            {
                return false;
            }
        }

        public bool CerrarConexion()
        {
            try
            {
                conexionDb.Close();
                return conexionDb.State == System.Data.ConnectionState.Closed;
            }
            catch(SqlException)
            {
                return false;
            }
        }

        public bool EjecutarCommando(string comando, SqlParameter[] parametros)
        {
            var comandoSql = new SqlCommand();

            comandoSql.CommandType = System.Data.CommandType.Text;
            comandoSql.CommandText = comando;
            comandoSql.Parameters.AddRange(parametros);

            AbrirConexion();
            comandoSql.Connection = conexionDb;

            var resultado = comandoSql.ExecuteNonQuery();
            CerrarConexion();

            return resultado > 0;
        }

        public DataSet Lectura(string comando)
        {
            var info = new DataSet();

            AbrirConexion();
            var adaptador = new SqlDataAdapter(comando, conexionDb);
            adaptador.Fill(info);
            CerrarConexion();

            return info;
        }

        public int EjecutarScalar(string comando, SqlParameter[] parametros)
        {
            var comandoSql = new SqlCommand();

            comandoSql.CommandType = System.Data.CommandType.Text;
            comandoSql.CommandText = comando;
            comandoSql.Parameters.AddRange(parametros);

            AbrirConexion();
            comandoSql.Connection = conexionDb;

            var resultado = (Int32) comandoSql.ExecuteScalar();
            CerrarConexion();

            return resultado;
        }
    }
}