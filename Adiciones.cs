using System;

namespace libreriaAlgoritmos
{
    public class Adiciones
    {
        public string OcultarTexto()
        {
            var texto = string.Empty;

            while(true)
            {
                ConsoleKeyInfo llave = Console.ReadKey(true);

                if (llave.Key == ConsoleKey.Enter)
                    break;

                else if(llave.Key == ConsoleKey.Backspace)
                {
                    if(texto.Length > 0)
                    {
                        texto = texto.Substring(0, (texto.Length - 1));
                        Console.Write("\b \b");
                    }
                }
                else if(llave.KeyChar != '\u0000')
                {
                    texto += llave.KeyChar;
                    Console.Write("*");
                }
            }

            return texto;
        }
    }
}