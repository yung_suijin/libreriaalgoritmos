using System.Collections.Generic;

namespace libreriaAlgoritmos.AccesoDatos
{
    public interface IAccesoDatos<T> where T : class
    {
        List<T> ObtenerListaTabla();
    }
}