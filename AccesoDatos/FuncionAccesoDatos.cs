using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using libreriaAlgoritmos.Entidades;

namespace libreriaAlgoritmos.AccesoDatos
{
    public interface IAccesoDatosFuncion : IAccesoDatos<Funcion>
    {
        bool EliminarFuncion(string nombrePelicula, string horarioInicio, string fecha, int sala);
        bool NuevaFuncion(Funcion nuevaFuncion);
    }

    public class FuncionAccesoDatos : IAccesoDatosFuncion
    {
        private string stringConexion = "Server=localhost; Database=ProyectoAlgoritmos; User Id=sa; Password=reallyStrongPwd123";
        private ConexionDb Conexion { get; set; }

        public FuncionAccesoDatos()
        {
            Conexion = new ConexionDb(stringConexion);
        }

        public bool EliminarFuncion(string nombrePelicula, string horarioInicio, string fecha, int sala)
        {
            var queryEliminar = $"DELETE FROM [dbo].[Funciones] WHERE NombrePelicula = @parametroNombre AND HorarioInicio = @parametroHorarioInicio AND Fecha = @parametroFecha " +
                "AND SalaID = @parametroSala";

            SqlParameter[] parametro =
            {
                new SqlParameter("@parametroNombre", SqlDbType.VarChar) { Value = nombrePelicula },
                new SqlParameter("@parametroHorarioInicio", SqlDbType.VarChar) { Value = horarioInicio },
                new SqlParameter("@parametroFecha", SqlDbType.VarChar) { Value = fecha },
                new SqlParameter("@parametroSala", SqlDbType.Int) { Value = sala }
            };

            return Conexion.EjecutarCommando(queryEliminar, parametro);
        }

        public bool NuevaFuncion(Funcion nuevaFuncion)
        {
            var queryInsertar = "INSERT INTO [dbo].[Funciones] (SalaID, PeliculaID, HorarioInicio, HorarioFinal, NombrePelicula, NombreSucursal, Fecha)" +
                " VALUES (@parametroSalaId, @parametroPeliculaId, @parametroHorarioInicio, @parametroHorarioFinal, @parametroNombrePelicula, @parametroNombreSucursal, @parametroFecha)";

            SqlParameter[] parametros =
            {
                new SqlParameter("@parametroSalaId", SqlDbType.Int) { Value = nuevaFuncion.IDSala },
                new SqlParameter("@parametroPeliculaId", SqlDbType.Int) { Value = nuevaFuncion.IDPelicula },
                new SqlParameter("@parametroHorarioInicio", SqlDbType.VarChar) { Value = nuevaFuncion.HorarioInicio },
                new SqlParameter("@parametroHorarioFinal", SqlDbType.VarChar) { Value = nuevaFuncion.HorarioFinal },
                new SqlParameter("@parametroNombrePelicula", SqlDbType.VarChar) { Value = nuevaFuncion.NombrePelicula },
                new SqlParameter("@parametroNombreSucursal", SqlDbType.VarChar) { Value = nuevaFuncion.NombreSucursal },
                new SqlParameter("@parametroFecha", SqlDbType.VarChar) { Value = nuevaFuncion.Fecha }
            };

            return Conexion.EjecutarCommando(queryInsertar, parametros);
        }

        public List<Funcion> ObtenerListaTabla()
        {
            var listaFunciones = new List<Funcion>();
            var querySeleccionar = "SELECT [SalaID],[PeliculaID],[HorarioInicio],[HorarioFinal],[NombrePelicula],[NombreSucursal],[Fecha] FROM [dbo].[Funciones]";

            var datos = Conexion.Lectura(querySeleccionar);

            foreach (DataRow row in datos.Tables[0].Rows)
            {
                var funcion = new Funcion();

                funcion.IDSala = Convert.ToInt32(row[0].ToString());
                funcion.IDPelicula = Convert.ToInt32(row[1].ToString());
                funcion.HorarioInicio = row[2].ToString();
                funcion.HorarioFinal = row[3].ToString();
                funcion.NombrePelicula = row[4].ToString();
                funcion.NombreSucursal = row[5].ToString();
                funcion.Fecha = row[6].ToString();

                listaFunciones.Add(funcion);
            }

            return listaFunciones;
        }

        public IEnumerable<Funcion> HorariosFuncionesSucursal(List<Funcion> lista, string nombreSucursal)
        {
            return from funciones in lista
                where funciones.NombreSucursal == nombreSucursal
                select funciones;
        }

        public IEnumerable<Funcion> TablaArbolFuncionesSala(List<Funcion> lista, int idSala)
        {
            return from funciones in lista
                where funciones.IDSala == idSala
                select funciones;
        }

        public int ContadorFuncionesMaximas(string nombrePelicula, string nombreSucursal)
        {
            var queryContador = "SELECT COUNT (*) FROM Funciones WHERE NombrePelicula = @parametroNombrePelicula AND NombreSucursal = @parametroNombreSucursal";

            SqlParameter[] parametros =
            {
                new SqlParameter("@parametroNombrePelicula", SqlDbType.VarChar) { Value = nombrePelicula },
                new SqlParameter("@parametroNombreSucursal", SqlDbType.VarChar) { Value = nombreSucursal }
            };

            return Conexion.EjecutarScalar(queryContador, parametros);
        }
    }
}