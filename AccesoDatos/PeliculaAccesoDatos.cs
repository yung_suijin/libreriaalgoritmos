using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using libreriaAlgoritmos.Entidades;

namespace libreriaAlgoritmos.AccesoDatos
{
    public interface IAccesoDatosPelicula : IAccesoDatos<Pelicula>
    {
        bool ModificarPelicula(Pelicula pelicula);
        bool EliminarPelicula(string nombrePelicula);
        bool NuevaPelicula(Pelicula nuevaPelicula);
    }

    public class PeliculaAccesoDatos : IAccesoDatosPelicula
    {
        private string stringConexion = "Server=localhost; Database=ProyectoAlgoritmos; User Id=sa; Password=reallyStrongPwd123";
        private ConexionDb Conexion { get; set; }

        public PeliculaAccesoDatos()
        {
            Conexion = new ConexionDb(stringConexion);
        }

        public bool EliminarPelicula(string nombrePelicula)
        {
            var queryEliminar = $"DELETE FROM [dbo].[Peliculas] WHERE NombrePelicula = @parametroNombre";

            SqlParameter[] parametro =
            {
                new SqlParameter("@parametroNombre", SqlDbType.VarChar) { Value = nombrePelicula }
            };

            return Conexion.EjecutarCommando(queryEliminar, parametro);
        }

        public bool ModificarPelicula(Pelicula pelicula)
        {
            var queryActualizar = $"UPDATE [dbo].[Peliculas] SET NombrePelicula = @parametroNombre, Director = @parametroDirector, Productor = @parametroProductor, " + 
                "Clasificacion = @parametroClasificacion, Duracion = @parametroDuracion, Genero = @parametroGenero WHERE NombrePelicula = @parametroNombre";

            var parametros = Parametros(pelicula);

            return Conexion.EjecutarCommando(queryActualizar, parametros);
        }

        public bool NuevaPelicula(Pelicula nuevaPelicula)
        {
            var queryInsertar = "INSERT INTO [dbo].[Peliculas] (NombrePelicula, Director, Productor, Clasificacion, Duracion, Genero)" +
                " VALUES (@parametroNombre, @parametroDirector, @parametroProductor, @parametroClasificacion, @parametroDuracion, @parametroGenero)";

            var parametros = Parametros(nuevaPelicula);

            return Conexion.EjecutarCommando(queryInsertar, parametros);
        }

        public List<Pelicula> ObtenerListaTabla()
        {
            var listaPeliculas = new List<Pelicula>();
            var querySeleccionar = "SELECT [PeliculaID],[NombrePelicula],[Director],[Productor],[Clasificacion],[Duracion],[Genero] FROM [dbo].[Peliculas]";

            var datos = Conexion.Lectura(querySeleccionar);

            foreach (DataRow row in datos.Tables[0].Rows)
            {
                var pelicula = new Pelicula();

                pelicula.IDPelicula = Convert.ToInt32(row[0]);
                pelicula.NombrePelicula = row[1].ToString();
                pelicula.Director = row[2].ToString();
                pelicula.Productor = row[3].ToString();
                pelicula.Clasificacion = row[4].ToString();
                pelicula.Duracion = Convert.ToInt32(row[5]);
                pelicula.Genero = row[6].ToString();

                listaPeliculas.Add(pelicula);
            }

            return listaPeliculas;
        }

        private SqlParameter[] Parametros(Pelicula pelicula)
        {
            SqlParameter[] parametros =
            {
                new SqlParameter("@parametroNombre", SqlDbType.VarChar) { Value = pelicula.NombrePelicula },
                new SqlParameter("@parametroDirector", SqlDbType.VarChar) { Value = pelicula.Director },
                new SqlParameter("@parametroProductor", SqlDbType.VarChar) { Value = pelicula.Productor },
                new SqlParameter("@parametroClasificacion", SqlDbType.VarChar) { Value = pelicula.Clasificacion },
                new SqlParameter("@parametroDuracion", SqlDbType.Int) { Value = pelicula.Duracion },
                new SqlParameter("@parametroGenero", SqlDbType.VarChar) { Value = pelicula.Genero }
            };

            return parametros;
        }
    }
}