using System.Collections.Generic;
using System.Data;
using libreriaAlgoritmos.Entidades;

namespace libreriaAlgoritmos.AccesoDatos
{
    public class SucursalAccesoDatos : IAccesoDatos<Sucursal>
    {
        private string stringConexion = "Server=localhost; Database=ProyectoAlgoritmos; User Id=sa; Password=reallyStrongPwd123";
        private ConexionDb Conexion { get; set; }

        public SucursalAccesoDatos()
        {
            Conexion = new ConexionDb(stringConexion);
        }

        public List<Sucursal> ObtenerListaTabla()
        {
            var listaSucursales = new List<Sucursal>();
            var querySeleccionar = "SELECT [EstadoID],[SucursalID],[NombreEstado],[NombreSucursal] FROM [dbo].[Sucursales]";

            var datos = Conexion.Lectura(querySeleccionar);

            foreach (DataRow row in datos.Tables[0].Rows)
            {
                var sucursal = new Sucursal();

                sucursal.IDEstado = row[0].ToString();
                sucursal.IDSucursal = row[1].ToString();
                sucursal.NombreEstado = row[2].ToString();
                sucursal.NombreSucursal = row[3].ToString();

                listaSucursales.Add(sucursal);
            }

            return listaSucursales;
        }
    }
}