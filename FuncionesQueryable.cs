using System.Collections.Generic;
using System.Linq;
using libreriaAlgoritmos.Entidades;

namespace libreriaAlgoritmos
{
    public class FuncionesQueryable
    {
        public IEnumerable<Funcion> FuncionEncontrada(List<Funcion> lista, string pelicula, int sala, string horario, string fecha)
        {
            return from funciones in lista
                where funciones.NombrePelicula == pelicula && funciones.IDSala == sala && 
                    funciones.HorarioInicio == horario && funciones.Fecha == fecha
                select funciones;
        }

        public IEnumerable<Cartelera> InfoFuncionesPelicula(List<Funcion> funciones, List<Pelicula> peliculas, string nombrePelicula, string nombreSucursal)
        {
            return from funcion in funciones
                   join pelicula in peliculas on funcion.NombrePelicula equals pelicula.NombrePelicula
                   where pelicula.NombrePelicula == nombrePelicula && funcion.NombreSucursal == nombreSucursal orderby funcion.Fecha ascending, funcion.HorarioInicio ascending
                   select Variables(funcion, pelicula);
        }

        public IEnumerable<Cartelera> InfoFuncionesClasificacion(List<Funcion> funciones, List<Pelicula> peliculas, string clasificacion, string nombreSucursal)
        {
            return from funcion in funciones
                   join pelicula in peliculas on funcion.NombrePelicula equals pelicula.NombrePelicula
                   where pelicula.Clasificacion == clasificacion && funcion.NombreSucursal == nombreSucursal orderby funcion.Fecha ascending, funcion.HorarioInicio ascending
                   select Variables(funcion, pelicula);
        }

        public IEnumerable<Cartelera> InfoFuncionesGenero(List<Funcion> funciones, List<Pelicula> peliculas, string genero, string nombreSucursal)
        {
            return from funcion in funciones
                   join pelicula in peliculas on funcion.NombrePelicula equals pelicula.NombrePelicula
                   where pelicula.Genero == genero && funcion.NombreSucursal == nombreSucursal orderby funcion.Fecha ascending, funcion.HorarioInicio ascending
                   select Variables(funcion, pelicula);
        }

        public IEnumerable<Funcion> FuncionesFecha(List<Funcion> funciones, string fecha)
        {
            return from funcion in funciones
                where funcion.Fecha == fecha
                select funcion;
        }

        public Cartelera Variables(Funcion funcion, Pelicula pelicula)
        {
            var cartelera = new Cartelera()
            {
                NombrePelicula = funcion.NombrePelicula,
                Fecha = funcion.Fecha,
                IDSala = funcion.IDSala,
                HorarioInicio = funcion.HorarioInicio,
                HorarioFinal = funcion.HorarioFinal,
                Director = pelicula.Director,
                Productor = pelicula.Productor,
                Duracion = pelicula.Duracion
            };

            return cartelera;
        }
    }
}